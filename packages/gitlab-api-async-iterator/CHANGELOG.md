# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.1.0 (2020-08-25)

### Bug Fixes

- **gitlab-api-async-iterator:** Deal with non-node environments ([6de110b](https://gitlab.com/leipert-projects/npm-packages/commit/6de110b5c1e55b1dead3673fad656f52f6b94e96))

### Features

- **gitlab-api-async-iterator:** Add Retry-After header to retry error handling ([ff18714](https://gitlab.com/leipert-projects/npm-packages/commit/ff18714c42e26515fb89822e3b7009cfc0de4603))

# 1.0.0 (2020-08-23)

### Features

- **gitlab-api-async-iterator:** Initial implementation ([133a4d1](https://gitlab.com/leipert-projects/npm-packages/commit/133a4d16ed257768a7f9cc8f9a91ee98005141eb))
