# `gitlab-api-async-iterator`

> Async iterator for GitLab API based on axios

## Usage

This module exposes a factory for creating async iterators for GitLab APIs.

This allows to use APIs pretty efficiently (code-wise).

It has a built-in retry feature in case you hit one of the following API errors: [429, 500, 502, 503, 504]

### Instantiation

```js
// es6 / webpack
import factory from "gitlab-api-async-iterator";
// node
const factory = require("gitlab-api-async-iterator");

const { GitlabAPI, GitLabAPIIterator } = factory({
  //default values
  baseURL: "https://gitlab.com/api/v4/",
  // If no token is defined, it tries to read process.env.GITLAB_TOKEN || process.env.DANGER_GITLAB_API_TOKEN
  privateToken: null,
  // How often certain failing requests are retried
  maxRetries: 5,
  // You can supply other options which would be provided to axios.create:
  // https://github.com/axios/axios#request-config
});
```

### Usage of the API

```js
let response;
// The GitLabAPI object is a normal instance of axios.create, so you could do:
response = await GitlabAPI.get("/user"); // Retrieve current user
response = await GitlabAPI.get("/version"); // Retrieve current GitLab version
response = await GitlabAPI.post("/projects", { name: "foobar" }); // Create a project
```

### Usage of the API Iterator

```js
// The real powerhouse is the GitLabAPIIterator:
const groupIterator = new GitLabAPIIterator("/groups");

// This will paginate through _all_ groups, and you have normal loop controls
for await (const group of groupIterator) {
  //Skip groups starting with 'A'
  if (group.name.startsWith("A")) {
    continue;
  }
  // Stop looping as soon as we hit a group starting with C
  if (group.name.startsWith("C")) {
    break;
  }
  console.log("Group Details:", group);
}

// You can provide options to the group iterator:
const aliceIterator = new GitLabAPIIterator("/users", {
  // General parameters:
  page: 2, // Start with page two
  maxPages: Number.MAX_SAFE_INTEGER, // how many pages to receive at a maximum
  per_page: 20, //Pagination size (GitLab default is 20)
  // Parameters specific to the endpoint
  search: "Alice",
});

for await (const alice of aliceIterator) {
  console.log("Found a Alice", alice);
}
```

### Subclassing the API Iterator:

```js
class PipelineIterator extends GitLabAPIIterator {
  constructor(projectId, options) {
    super(`/projects/${projectId}/pipelines`, options);
  }
}

class PipelineJobIterator extends GitLabAPIIterator {
  constructor(projectId, pipelineId, options) {
    super(`/projects/${projectId}/pipelines/${pipelineId}/jobs`, options);
  }
}

const projectId = "foobar";

const pipelines = new PipelineIterator(projectId);
for await (const pipeline of pipelines) {
  console.log(pipeline);

  const jobs = new PipelineIterator(projectId, pipeline.id);

  let found = false;

  for await (const job of jobs) {
    if (job.name.includes("jest")) {
      found = true;
      break;
    }
  }

  if (found) {
    console.log(`${pipeline.web_url} is the first Pipeline with a jest job`);
    break;
  }
}
```
