module.exports = (gitlabAPI) =>
  class GitLabAPIIterator {
    constructor(url, options = {}) {
      this.url = url;
      this.options = options;
      this.page = options.page || 1;
      this.maxPages = options.maxPages || Number.MAX_SAFE_INTEGER;
    }

    async *[Symbol.asyncIterator]() {
      while (this.page < this.maxPages) {
        const { data } = await gitlabAPI.get(this.url, {
          params: {
            ...this.options,
            page: this.page,
          },
        });

        if (!data.length) {
          break;
        }

        for (let value of data) {
          yield value;
        }

        this.page += 1;
      }
    }
  };
