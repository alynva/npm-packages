const axios = require("axios");

const timeout = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

const requestMethods = [
  "request",
  "get",
  "delete",
  "head",
  "options",
  "post",
  "put",
  "patch",
];

const waitBeforeRetry = async (error, retries) => {
  // Be forgiving if we hit server errors or rate limits and retry after some time
  if (
    error &&
    error.response &&
    [429, 500, 502, 503, 504].includes(error.response.status)
  ) {
    let wait;

    const log = [`Got a status code of ${error.response.status}.`];

    if (error.response.headers) {
      try {
        const {
          "ratelimit-reset": rateLimitReset,
          "ratelimit-resettime": rateLimitResetTime,
          "retry-after": retryAfter,
        } = error.response.headers;

        if (rateLimitReset) {
          wait = parseInt(rateLimitReset, 10) * 1000 - new Date().getTime();

          log.push(
            `Found a ratelimit-reset header, waiting until ${rateLimitResetTime}.`
          );
        }

        if (retryAfter) {
          wait = parseInt(retryAfter, 10) * 1000;

          log.push(`Found a retry-after header (${retryAfter}).`);
        }
      } catch (e) {}
    }

    if (!wait) {
      wait = 2 ** retries * 100;
    }

    log.push(`Waiting ${wait} ms`);
    console.warn(log.join(" "));
    await timeout(wait);
    return retries + 1;
  } else {
    throw error;
  }
};

const getTokenFromProcess = () =>
  process &&
  process.env &&
  (process.env.GITLAB_TOKEN || process.env.DANGER_GITLAB_API_TOKEN);

const GitLabAPIFactory = (options = {}) => {
  const {
    baseURL = "https://gitlab.com/api/v4/",
    privateToken = null,
    maxRetries = 5,
    ...axiosOptions
  } = options;

  if (!privateToken && !getTokenFromProcess()) {
    throw new Error(
      "Please provide a GitLab token as an ENV variable via GITLAB_TOKEN or DANGER_GITLAB_API_TOKEN"
    );
  }

  const { headers, ...rest } = axiosOptions;

  const _gitlabAPI = axios.create({
    ...rest,
    baseURL,
    headers: {
      ...headers,
      "PRIVATE-TOKEN": privateToken || getTokenFromProcess(),
    },
  });

  return new Proxy(_gitlabAPI, {
    get(target, prop) {
      if (requestMethods.includes(prop)) {
        return async (...args) => {
          let retries = 0;

          do {
            try {
              return await target[prop](...args);
            } catch (e) {
              if (retries >= maxRetries) {
                throw e;
              }
              retries = await waitBeforeRetry(e, retries);
            }
          } while (true);
        };
      }

      return target[prop];
    },
  });
};

module.exports = GitLabAPIFactory;
