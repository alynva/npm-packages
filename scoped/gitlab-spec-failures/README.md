# `@leipert/gitlab-spec-failures`

> Check which specs failed in a gitlab-org/gitlab pipeline

## Usage

```
yarn global add @leipert/gitlab-spec-failures

# Ensure you define a GITLAB_TOKEN as a env variable

gitlab-spec-failures https://gitlab.com/gitlab-org/gitlab/-/merge_requests/33252
# or:
gitlab-spec-failures https://gitlab.com/gitlab-org/gitlab/-/pipelines/177609114
```
