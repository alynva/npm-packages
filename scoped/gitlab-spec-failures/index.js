#!/usr/bin/env node
const readline = require("readline");
const {
  GitLabAPI: gitlabAPI,
  GitLabAPIIterator,
} = require("gitlab-api-async-iterator")();

class JobIterator extends GitLabAPIIterator {
  constructor(projectPath, pipelineId) {
    super(
      `/projects/${encodeURIComponent(
        projectPath
      )}/pipelines/${pipelineId}/jobs`
    );
  }
}

const SECTION_START = /section_start:.+:step_script/;
const SECTION_END = /section_end:.+:step_script/;

async function processTrace(stream, processor) {
  const rl = readline.createInterface({
    input: stream,
    crlfDelay: Infinity,
  });
  let started = false;
  let response = [];
  for await (const line of rl) {
    if (!started) {
      started = SECTION_START.test(line);
      continue;
    }
    response.push(processor(line));

    if (SECTION_END.test(line)) {
      rl.close();
      return response;
    }
  }
}

function sortUnique(arr) {
  return [...new Set(arr)].sort();
}

function printUsage() {
  return [
    "Usage: gitlab-spec-failures <pipeline_web_url | merge_request_web_url> ",
    "",
    "Please provide a URL of the format: https://gitlab.com/<project_path>/-/pipelines/<pipeline_id>",
    " or https://<project_path>/gitlab/-/merge_requests/<mr_iid>",
  ].join("\n");
}

async function main() {
  const GitLabMRRegex = /https:\/\/gitlab.com\/(.+)\/-\/merge_requests\/(\d+)/;
  const GitLabPipelineRegex = /https:\/\/gitlab.com\/(.+)\/-\/pipelines\/(\d+)/;

  const mrURL = process.argv.find((x) => GitLabMRRegex.test(x));
  const pipelineURL = process.argv.find((x) => GitLabPipelineRegex.test(x));

  if (!pipelineURL && !mrURL) {
    console.warn(printUsage());
    throw new Error("URL argument missing");
  }

  let projectPath, pipelineId, mrIid;

  if (pipelineURL) {
    [, projectPath, pipelineId] = pipelineURL.match(GitLabPipelineRegex);
    console.log(`Retrieving data for Pipeline: ${pipelineURL}`);
  } else {
    [, projectPath, mrIid] = mrURL.match(GitLabMRRegex);
    console.log(`Retrieving data for MR: ${mrURL}`);
    const { data: pipelines } = await gitlabAPI.get(
      `/projects/${encodeURIComponent(
        projectPath
      )}/merge_requests/${mrIid}/pipelines`
    );
    if (!pipelines.length) {
      throw new Error(`MR ${mrURL} doesn't have a pipeline yet`);
    }
    pipelineId = pipelines.sort((b, a) => a.id - b.id)[0].id;
    console.log(
      `Found ${pipelines.length} pipelines, latest seems to be: ${pipelineId}`
    );
  }

  const iterator = new JobIterator(projectPath, pipelineId);

  const jestFailures = [];

  const rspecFailures = [];

  let running = false;

  for await (const job of iterator) {
    const { id, name, web_url, status } = job;

    if (
      name.startsWith("rspec-ee ") ||
      name.startsWith("rspec ") ||
      name.startsWith("jest ")
    ) {
      if (["pending", "running"].includes(status)) {
        running = true;
        console.log(
          `'${name}' is not finished yet, status: ${status} (${web_url})`
        );
        continue;
      }
      if (status === "success") {
        continue;
      }
      const traceURL = `/projects/${encodeURIComponent(
        projectPath
      )}/jobs/${id}/trace`;
      console.log(
        `Retrieving trace for: '${name}', status: ${status} (${web_url})`
      );
      const { data: traceStream } = await gitlabAPI.get(traceURL, {
        responseType: "stream",
      });
      if (name.startsWith("jest")) {
        jestFailures.push(
          ...(
            await processTrace(traceStream, (line) => line.match(/FAIL (\S+)/))
          )
            .filter(Boolean)
            .map((x) => x[1])
        );
      } else {
        rspecFailures.push(
          ...(
            await processTrace(traceStream, (line) =>
              line.match(/rspec (.+?):/)
            )
          )
            .filter(Boolean)
            .map((x) => x[1])
        );
      }
    }
  }

  const failedJestFiles = sortUnique(jestFailures);
  const failedRspecFiles = sortUnique(rspecFailures);

  console.log("# ".repeat(60));

  if (failedJestFiles.length > 0) {
    console.log(
      `Jest command to run ${failedJestFiles.length} failing files:\n`
    );

    console.log("yarn run jest -u --watch " + failedJestFiles.join(" \\\n  "));
  } else {
    console.log(`No jest failures found in pipeline ${pipelineId} - hooray!`);
  }

  console.log("# ".repeat(60));

  if (failedRspecFiles.length > 0) {
    console.log(
      `Rspec command to run ${failedRspecFiles.length} failing files:\n`
    );

    console.log("./bin/spring rspec " + failedRspecFiles.join(" \\\n  "));
  } else {
    console.log(`No rspec failures found in pipeline ${pipelineId} - hooray!`);
  }

  if (running) {
    console.log("At least one job is still running (see above)");
  }
}

if (process.argv.includes("--help" || "-h")) {
  console.log(printUsage());
  process.exit(0);
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
