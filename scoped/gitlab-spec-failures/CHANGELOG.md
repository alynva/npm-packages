# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.0.0 (2020-08-25)

### Bug Fixes

- **gitlab-spec-failures:** Fix output of jest and rspec failures ([2ac703f](https://gitlab.com/leipert-projects/npm-packages/commit/2ac703f25b3b6756f4b8a690a952b360cadda84a))

### Features

- **gitlab-spec-failures:** Initial implementation ([52767d6](https://gitlab.com/leipert-projects/npm-packages/commit/52767d65481575cf0359b481c610ae86d47f5c77))
